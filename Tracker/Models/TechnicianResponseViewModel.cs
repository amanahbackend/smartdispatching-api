﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracker.Models
{
    public class TechnicianResponseViewModel
    {
        public int OrderId { get; set; }
        public int ResponseId { get; set; }
    }
}