﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracker.Models
{
	public class DeviceModel
	{
		public int techId { get; set; }
		public string deviceId	 { get; set; }
		public string type { get; set; }
	}
}