﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracker.Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        public string PACI { get; set; }
        public string Area { get; set; }
        public string Problem { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string OrderNo { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerPhoneTwo { get; set; }
        public string CreatedDate { get; set; }
        public decimal? Long { get; set; }
        public decimal? Lat { get; set; }
        public int? ActionId { get; set; }
        public int IsTechSeen { get; set; }
        public string ActionName { get; set; }
        public string ContractNo { get; set; }

    }
}