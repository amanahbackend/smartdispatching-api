﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracker.Models
{
	public enum Action
	{
		NoAction = 1,
		Received = 2,
		Started = 3,
		Finished = 4
	}
}