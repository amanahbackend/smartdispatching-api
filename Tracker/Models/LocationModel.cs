﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracker.Models
{
	public class LocationModel
	{
		public decimal longitude { get; set; }
		public decimal latitude { get; set; }
	}
}