﻿using System.Collections.Generic;

namespace Tracker.Models
{
    public class PrintOutGettingModel
    {
        public List<int> statusIds { get; set; }
        public List<int> formanIds { get; set; }
        public List<PrintTechModel> techIds { get; set; }
        public bool includeUnassign { get; set; }
        public bool includeInprogress { get; set; }
        public string orderNo { get; set; }
        public string dispatcherId { get; set; }
    }
}