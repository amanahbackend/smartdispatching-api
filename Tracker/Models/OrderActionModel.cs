﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tracker.Models
{
	public class OrderActionModel
	{
		public int actionId { get; set; }
		public decimal longitude { get; set; }
		public decimal latitude { get; set; }
		public string techId { get; set; }
		public int orderId { get; set; }
	}
}