﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using Tracker.DB.Database;
using Tracker.Models;

namespace Tracker.Controllers
{
    [RoutePrefix("api/Orders")]
    public class OrdersController : ApiController
    {
        Dispatching dbContext = new Dispatching();
        enum IsTechSeen : int { IsSent = 0, IsAccept = 1, IsReject = 2 };


        [Authorize]
        [Route("")]
        public IHttpActionResult Get()
        {
            List<OrderModel> TechOrders = new List<OrderModel>();
            var techName = User.Identity.Name;
            var TechId = dbContext.Technicians.Where(x => x.Name == techName).FirstOrDefault().Id;
            var AssignedOrders = dbContext.AssignedOrders.Where(x => x.TechnicianId == TechId && x.IsClosed == false).ToList();
            var OrdersId = new List<int?>();
            for (int i = 0; i < AssignedOrders.Count; i++)
            {
                OrdersId.Add(AssignedOrders[i].OrderId);
            }
            var Orders = dbContext.Orders.Where(x => OrdersId.Contains(x.Id)).ToList();
            for (int i = 0; i < Orders.Count; i++)
            {
                var customerId = Orders[i].CustomerId;
                var contract = dbContext.Contracts.Where(x => x.CustomerId == customerId).OrderBy(x => x.CreatedDate).ToList().LastOrDefault();
                OrderModel temp = new OrderModel()
                {
                    Id = Orders[i].Id,
                    CreatedDate = Orders[i].CreatedDate.ToString(),
                    CustomerName = Orders[i].Customer.Name,
                    Type = Orders[i].OrderType.Name,
                    CustomerPhone = Orders[i].Customer.PhoneOne,
                    CustomerPhoneTwo = Orders[i].Customer.PhoneTwo,
                    OrderNo = Orders[i].OrderNo,
                    ContractNo = contract?.ContractNo,
                    Status = Orders[i].OrderStatu.Name,
                    Problem = Orders[i].Problem.Name,
                    Area = Orders[i].Address.Area.Name,
                    PACI = Orders[i].Address.PACI,
                    Lat = Orders[i].Address.Lat,
                    Long = Orders[i].Address.Long,
                    ActionId = 1,
                };
                var ass = Orders[i].AssignedOrders.FirstOrDefault();
                var action = dbContext.OrderActions.OrderBy(x => x.Id).FirstOrDefault();
                if (ass.ActionId != null)
                {
                    var isAction = dbContext.OrderActions.Where(x => x.Id == ass.ActionId).FirstOrDefault();
                    temp.ActionId = ass.ActionId;
                    temp.ActionName = isAction.Name;
                    if (ass.IsAction == null)
                    {
                        if (temp.ActionId == null)
                        {
                            temp.ActionId = 1;
                            dbContext.SaveChanges();
                            ass.IsAction = 0;
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            ass.IsAction = 0;
                            dbContext.SaveChanges();
                        }
                      
                    }
                    temp.IsTechSeen = (int)ass.IsAction;
                }
                else
                {
                    temp.ActionId = 1;
                    temp.ActionName = action.Name;
                    temp.IsTechSeen = (int)IsTechSeen.IsSent;
                }

                TechOrders.Add(temp);
            }
            return Ok(TechOrders);
        }



        [Authorize]
        [Route("{Id}")]
        public IHttpActionResult Get(int Id)
        {
            OrderModel temp = new OrderModel();
            var Order = dbContext.Orders.Where(x => x.Id == Id).FirstOrDefault();

            if (Order != null)
            {
                var contract = dbContext.Contracts.Where(x => x.CustomerId == Order.CustomerId).OrderBy(x => x.CreatedDate).ToList().LastOrDefault();

                temp = new OrderModel()
                {
                    Id = Order.Id,
                    CreatedDate = Order.CreatedDate.ToString(),
                    CustomerName = Order.Customer.Name,
                    CustomerPhone = Order.Customer.PhoneOne,
                    OrderNo = Order.OrderNo,
                    Status = Order.OrderStatu.Name,
                    Problem = Order.Problem.Name,
                    Area = Order.Address.Area.Name,
                    PACI = Order.Address.PACI,
                    Lat = Order.Address.Lat,
                    Long = Order.Address.Long,
                    ActionId = 1,
                    ContractNo = contract?.ContractNo,
                    Type = Order.OrderType.Name
                };
                var ass = Order.AssignedOrders.FirstOrDefault();
                var action = dbContext.OrderActions.OrderBy(x => x.Id).FirstOrDefault();
                if (ass.ActionId != null)
                {
                    var isAction = dbContext.OrderActions.Where(x => x.Id == ass.ActionId).FirstOrDefault();
                    temp.ActionId = ass.ActionId;
                    temp.ActionName = isAction.Name;
                    temp.IsTechSeen = (int)ass.IsAction;
                }
                else
                {
                    temp.ActionId = 1;
                    temp.ActionName = action.Name;
                    temp.IsTechSeen = (int)IsTechSeen.IsSent;
                }

                return Ok(temp);
            }
            return Ok(temp);
        }

        [Authorize]
        [Route("Action")]
        [HttpPost]
        public IHttpActionResult TechnicianAction([FromBody] OrderActionModel orderAction)
        {

            var user = dbContext.AspNetUsers.Where(x => x.Id == orderAction.techId).FirstOrDefault();

            var techId = dbContext.Technicians.Where(x => x.Name == user.UserName).FirstOrDefault().Id;

            if (orderAction != null)
            {
                var assignedOrder = dbContext.AssignedOrders.Where(x => x.TechnicianId == techId && x.OrderId == orderAction.orderId).FirstOrDefault();
                if (assignedOrder != null)
                {
                    assignedOrder.ActionDate = DateTime.Now;
                    assignedOrder.ActionId = orderAction.actionId;
                    assignedOrder.Long = orderAction.longitude;
                    assignedOrder.Lat = orderAction.latitude;
                    dbContext.SaveChanges();
                    return Created("success", "Action updated");
                }
                else
                {
                    return NotFound();
                }
            }
            return BadRequest("please send order action object");
        }

        [Authorize]
        [Route("GetStatuses")]
        public IHttpActionResult GetStatuses()
        {

            List<StatusViewModel> result = new List<StatusViewModel>();
            var statuses = dbContext.OrderStatus.ToList();
            if (statuses != null && statuses.Count > 0)
            {
                for (int i = 0; i < statuses.Count; i++)
                {
                    StatusViewModel status = new StatusViewModel()
                    {
                        Id = statuses[i].Id,
                        Name = statuses[i].Name
                    };
                    result.Add(status);
                }
            }

            return Ok(result);
        }

        [Authorize]
        [Route("GetReasons")]
        public IHttpActionResult GetReasons()
        {

            List<ReasonsViewModel> result = new List<ReasonsViewModel>();
            var reasons = dbContext.Reasons.ToList();
            if (reasons != null && reasons.Count > 0)
            {
                for (int i = 0; i < reasons.Count; i++)
                {
                    ReasonsViewModel reason = new ReasonsViewModel()
                    {
                        Id = reasons[i].Id,
                        Name = reasons[i].Name
                    };
                    result.Add(reason);
                }
            }

            return Ok(result);
        }

        [Authorize]
        [Route("GetActions")]
        public IHttpActionResult GetActions()
        {

            List<StatusViewModel> result = new List<StatusViewModel>();
            var statuses = dbContext.OrderActions.ToList();
            if (statuses != null && statuses.Count > 0)
            {
                for (int i = 0; i < statuses.Count; i++)
                {
                    StatusViewModel status = new StatusViewModel()
                    {
                        Id = statuses[i].Id,
                        Name = statuses[i].Name
                    };
                    result.Add(status);
                }
            }

            return Ok(result);
        }


        public class TechnicianResponseViewModel
        {
            public int OrderId { get; set; }
            public int ResponseId { get; set; }
            public int ReasonId { get; set; }
        }

        [Authorize]
        [Route("SetTechnicianResponse")]
        [HttpPost]
        public IHttpActionResult SetTechnicianResponse([FromBody] TechnicianResponseViewModel technicianResponse)
        {
            if (technicianResponse.ResponseId==2 && technicianResponse.ReasonId==0)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "There is no rejection reason"));
            }
            var orderAssigned = dbContext.AssignedOrders.Where(x => x.OrderId == technicianResponse.OrderId).FirstOrDefault();
            orderAssigned.IsAction = technicianResponse.ResponseId;
            orderAssigned.ReasonId = technicianResponse.ReasonId;
            dbContext.SaveChanges();
            return Ok();
        }

        [Authorize]
        [Route("GetTechnicianResponse")]
        public IHttpActionResult GetTechnicianResponse()
        {
            List<dynamic> result = new List<dynamic>();

            dynamic accept = new System.Dynamic.ExpandoObject();
            accept.Id = 1;
            accept.Name = "Accept";
            dynamic reject = new System.Dynamic.ExpandoObject();

            reject.Id = 2;
            reject.Name = "Reject";

            result.Add(accept);
            result.Add(reject);

            return Ok(result);
        }

    }
}