﻿using System.Web.Http;
using Tracker.DB.Database;
using System.Collections.Generic;
using System.Linq;
using Tracker.Models;
using System;
using System.Data.Entity.Validation;

namespace Tracker.Controllers
{
	[RoutePrefix("api/Technicians")]
	public class TechniciansController : ApiController
	{
		Dispatching dbContext = new Dispatching();

		[Authorize]
		[Route("Info")]
		public IHttpActionResult Get()
		{
			var techName = User.Identity.Name;
			var techData = dbContext.Technicians.Where(x => x.Name == techName).FirstOrDefault();
			TechnicianModel outTech = new TechnicianModel()
			{
				Id = techData.Id,
				Name = techData.Name
			};
			return Ok(outTech);
		}

		[Authorize]
		[Route("Movement")]
		[HttpPost]
		public IHttpActionResult AddMovement([FromBody] LocationModel location)
		{
			if (location != null)
			{
				if (location.latitude == 0 && location.longitude == 0)
				{
					return BadRequest("latitude & longitude have invalid values");
				}
				var techName = User.Identity.Name;
				var techData = dbContext.Technicians.Where(x => x.Name == techName).FirstOrDefault();

				TechnicianMovement temp = new TechnicianMovement()
				{
					TrackingDate = DateTime.Now,
					TechnicianId = techData.Id,
					Lat = location.latitude,
					Long = location.longitude
				};
				dbContext.TechnicianMovements.Add(temp);
				dbContext.SaveChanges();

				temp.Technician = null;

				return Created("success", temp);
			}
			return BadRequest("please send location object");
		}

		[Authorize]
		[Route("RegisterDevice")]
		[HttpPost]
		public IHttpActionResult RegisterDevice([FromBody] DeviceModel device)
		{
			if (device != null)
			{
				var tempDevice = dbContext.Devices.Where(x => x.TechId == device.techId).FirstOrDefault();
				if (tempDevice != null)
				{
					tempDevice.DeviceId = device.deviceId;
					tempDevice.Type = device.type;
					dbContext.SaveChanges();
				}
				else
				{
					tempDevice = new Device()
					{
						DeviceId = device.deviceId,
						TechId = device.techId,
						Type = device.type
					};

					dbContext.Devices.Add(tempDevice);
					dbContext.SaveChanges();
				}
				return Ok("Success");
			}
			return BadRequest("please send device object");
		}
	}
}
