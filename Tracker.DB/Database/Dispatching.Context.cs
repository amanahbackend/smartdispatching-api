﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tracker.DB.Database
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Dispatching : DbContext
    {
        public Dispatching()
            : base("name=Dispatching")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<AssignedOrder> AssignedOrders { get; set; }
        public virtual DbSet<Block> Blocks { get; set; }
        public virtual DbSet<Cancellation_Reasons> Cancellation_Reasons { get; set; }
        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<CompanyCode> CompanyCodes { get; set; }
        public virtual DbSet<Contract> Contracts { get; set; }
        public virtual DbSet<ContractType> ContractTypes { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Device> Devices { get; set; }
        public virtual DbSet<DispatcherArea> DispatcherAreas { get; set; }
        public virtual DbSet<DispatcherCombination> DispatcherCombinations { get; set; }
        public virtual DbSet<DispatcherData> DispatcherDatas { get; set; }
        public virtual DbSet<DispatcherDivision> DispatcherDivisions { get; set; }
        public virtual DbSet<DispatcherOrder> DispatcherOrders { get; set; }
        public virtual DbSet<DispatcherProblem> DispatcherProblems { get; set; }
        public virtual DbSet<Division> Divisions { get; set; }
        public virtual DbSet<Forman> Formen { get; set; }
        public virtual DbSet<FormanArea> FormanAreas { get; set; }
        public virtual DbSet<FormanProblem> FormanProblems { get; set; }
        public virtual DbSet<Governorate> Governorates { get; set; }
        public virtual DbSet<Material> Materials { get; set; }
        public virtual DbSet<NotificationTime> NotificationTimes { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderAction> OrderActions { get; set; }
        public virtual DbSet<OrderPriority> OrderPriorities { get; set; }
        public virtual DbSet<OrderStatu> OrderStatus { get; set; }
        public virtual DbSet<OrderType> OrderTypes { get; set; }
        public virtual DbSet<OrderWorkUpdate> OrderWorkUpdates { get; set; }
        public virtual DbSet<Problem> Problems { get; set; }
        public virtual DbSet<RowData> RowDatas { get; set; }
        public virtual DbSet<SAPArea> SAPAreas { get; set; }
        public virtual DbSet<Street_Jaddah> Street_Jaddah { get; set; }
        public virtual DbSet<SuperDispatcher> SuperDispatchers { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<Technician> Technicians { get; set; }
        public virtual DbSet<TechnicianAction> TechnicianActions { get; set; }
        public virtual DbSet<TechnicianArea> TechnicianAreas { get; set; }
        public virtual DbSet<TechnicianMovement> TechnicianMovements { get; set; }
        public virtual DbSet<TechnicianProblem> TechnicianProblems { get; set; }
        public virtual DbSet<TV_Availability> TV_Availability { get; set; }
        public virtual DbSet<UsedMaterialHistory> UsedMaterialHistories { get; set; }
        public virtual DbSet<Work_Update> Work_Update { get; set; }
        public virtual DbSet<Reason> Reasons { get; set; }
    }
}
